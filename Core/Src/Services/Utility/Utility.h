/****************************************************************************************************/
/**
  \file         Address.h
  \brief        Utilities as CRC, global const, errors definitions, address of external memories.
  \author       Am�rico Lorenzana Guti�rrez
  \project      IKMaster1.0
  \version
  \date         Jun 6, 2012

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/
#ifndef __UTILITY_H__
#define __UTILITY_H__

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "Typedefs.h"
//#include "stm32f10x.h"
//#include <string.h>

/* Kernel includes. */
//#include "FreeRTOS.h"
//#include "task.h"
//#include "queue.h"
//#include "timers.h"
//#include "semphr.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
/********************************
 * Internal communication Enums *
 ********************************/
typedef enum
{
	eTASK1 = 0,
	eTASK2,
	eTASK3,
	eTASK_MAX
}teTask;

typedef enum
{
	eINT_FLAG_NONE = 0,
	eINT_FLAG_MASTER_BUFFER,
	eINT_FLAG_ACTION,
	eINT_FLAG_EXECUTE,
	eINT_FLAG_EXECUTE_OK,
	eINT_FLAG_EXECUTE_NOK,
	eINT_FLAG_DATA,
	eINT_FLAG_DATA_OK,
	eINT_FLAG_DATA_NOK,
	eINT_FLAG_MAX
}teFlags;

typedef enum
{
	eINT_CMD_NONE = 0,
	eINT_CMD_EXT,
	eINT_CMD_INT_1,
	eINT_CMD_INT_2,
	eINT_CMD_INT_3,
	eINT_CMD_MAX
}teIntCmd;

typedef enum
{
	eINT_STATUS_OK = 0,
	eINT_STATUS_UNDEFINED,
	eINT_STATUS_ERROR_ACTION,
	eINT_STATUS_ERROR_ALLOWED_ACTION,
	eINT_STATUS_ERROR_TIMEOUT,
	eINT_STATUS_ERROR_BAD_TASK,
	eINT_STATUS_ERROR_DATA_POINTER,
	eINT_STATUS_ERROR_EXECUTE,
	eINT_STATUS_ERROR_DATA,
	eINT_STATUS_MAX
}teIntStatus;

/*************************************
 * Internal communication Structures *
 *************************************/

/* Inter-tasks communication. Queue's type */
typedef struct
{
	u8			eTaskOrig	: 4;
	u8			eTaskDest	: 4;
	u8			eFlag;
	u8			eIntCmd;
}tsInfo;

typedef struct
{
	tsInfo		sInfo;
	u16			u16Size;
	u8			*pu8Data;
}tsMsg;

//typedef struct
//{
//	tsInfo		sInfo;
//	teIntCmd	eIntCmd;
//	u8			padding;
//}tsMsgN;

//typedef struct
//{
//	teTask			eTask;
//	TaskHandle_t	xTask;
//}tsTasks;

/********************************
 * External communication Enums *
 ********************************/
typedef enum
{
	eCMD_NONE = 0,
	eCMD_1,
	eCMD_2,
	eCMD_MAX
}teCmd;

typedef enum
{
	eSTATUS_OK = 0,
	eSTATUS_NOK,
	eSTATUS_MAX
}teStatus;
/*************************************
 * External communication Structures *
 *************************************/
typedef struct
{
	u8			u8Dest;
	u8			u8Orig;
	u8			u8Cmd;
	teStatus	eStatus;
	u16			u16Size;
}tsHeader;
/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               Extern                                               *
*****************************************************************************************************/

/*****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
//TaskHandle_t xfnUtilityTasks(teTask eTask);
u16 u16fnUtilityCrc (u8 *ptFRAME, u16 wSizeFrame, u16 wCRCStart);
u32 u32fnUtilityCrc(const void* data, u16 length, u32 previousCrc32);
u16 u16fnUtilityCmd(tsHeader *psHeader, teStatus eStatus, u16 u16Size, u8 *pu8Data);
void vfnUtilityMsg(tsMsg *psMsg, teFlags eFlag, teCmd eCmd);
teIntStatus efnUtilitySend(tsMsg *psMsg, QueueHandle_t xQueue, u32 u32Timeout, u8 u8ErrorCount);

#endif  /*__UTILITY_H__*/
/***************************************End of File**************************************************/


